# Gabriel Lourenco - Resume
Proyecto para control de mi [**resumen profesional**](https://lourencocristian.gitlab.io/resume/)
y su publicación como sitio web estático utilizando [*Gitlab Pages*](https://docs.gitlab.com/ee/user/project/pages/)

> Basado en un *fork* del proyecto de [Luis Fuentes](https://gitlab.com/luisfuentes/resume).
Gracias @luisfuentes por la contribución

## Uso 🚀

1. Realiza un *fork* de este repositorio. 

2. Modifica los datos de [**data.json**](data.json) con tu información. 

3. Sube los combios en *Gitlab*. 

4. Tu resumen ya esta publicado y disponible la dirección es `https://{usuario_gitlab}.gitlab.io/resume/` 
Puedes ver un ejemplo de como quedará en el mio [https://lourencocristian.gitlab.io/resume/](https://lourencocristian.gitlab.io/resume/)

### Integración continua (CI)
El proyecto cuenta con una configuración de integración continua de [***Gitlab***](.gitlab-ci.yml) que permite tener la revisión de cambios en publicaciones del sitio por cada rama de trabajo. 

Sobre cada [***Merge Request***](https://gitlab.com/lourencocristian/resume/-/merge_requests) se puede acceder a la publicación de esa rama para revisar los cambios antes de incluirlos a *master* y que sean publicados en el sitio productivo.

## Personaliza el estilo 💅🏻

- **Estilo:** para cambiar colores, tipos de letra y demas edita [**style.css**](public/styles.css) 
en la carpeta *public* o el archivo [**mustache**](index.mustache) para cambiar
el *layout*.
- **Foto de perfil:** Yo recomiendo utilizar [**Gravatar**](https://gravatar.com/)
así como lo hace *Gitlab* para tu perfil. Sigue [**estas instrucciones**](https://es.gravatar.com/site/implement/images/)
- **QR de contacto:** para una presentación *geek* reemplaza el archivo [**QR file**](public/QR.png) (Ej: http://qr-code-generator.com)
Tamaño recomendable `150 x 150 px`
- **Dominio:** si tienes y prefieres que tu resumen este publicado con un dominio
propio Gitlab Pages te permite hacerlo. Sigue [estas instrucciones](https://gitlab.com/help/user/project/pages/custom_domains_ssl_tls_certification/index.md#set-up-pages-with-a-custom-domain)
Mi caso [**https://gabriellourenco.com/**](https://gabriellourenco.com/)

### Google Analytics
Si quieres monitorear tu sitio personal puedes configurar [***Google Analytics***](https://analytics.google.com/) modificando el ***ID*** en el [index del proyecto](index.mustache#L4)

### License©️
No te preocupes por la licencia, orginalmente parte del diseño de [**Franklin Schamhart**](https://dribbble.com/shots/1887983-Resume).

Esta bajo ***MIT Open Source license***. Para mas información revisa el archivo
en el proyecto.
