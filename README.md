# Gabriel Lourenco - Resume
Project for my professional [**resume**](https://lourencocristian.gitlab.io/resume/)
and their publication with [**Gitlab Pages**](https://docs.gitlab.com/ee/user/project/pages/)

> Based in a fork from [Luis Fuentes](https://gitlab.com/luisfuentes/resume) 
project. Thanks @luisfuentes for your contribution.

## Quick Start 🚀

1. Fork this repository. 

2. Modify [**data.json**](data.json) with your information. 

3. Push your changes to Gitlab. 

Now your resume is public on `https://{username}.gitlab.io/resume/` 
My example is here [https://lourencocristian.gitlab.io/resume/](https://lourencocristian.gitlab.io/resume/)

### Continuos Integration
The project has a continuous integration configuration with [***Gitlab***](.gitlab-ci.yml), that allows has to review application in each branch.

From [***Merge Requests***](https://gitlab.com/lourencocristian/resume/-/merge_requests) you can view a diferent publish application to review cahnges before merge into *master* and publish to production.

## Customize your resume 💅🏻

- **Style:** to add your style just modify the [**style.css**](public/styles.css) 
file in the public folder or the [**mustache template file**](index.mustache) to
change the layout.
- **Profile photo:** I recommended using the [**Gravatar**](https://gravatar.com/)
profile as Gitlab uses for the profile. To do this follow [**these instructions**](https://es.gravatar.com/site/implement/images/)
- **Contact QR:** tech design generate *QR* code with your information and replace [**QR file**](public/QR.png)
Recommended size `150 x 150 px`
- **Domain:** if you wish Gitlab Pages allow to customize adding a domain what 
you manage. Follow [these instructions](https://gitlab.com/help/user/project/pages/custom_domains_ssl_tls_certification/index.md#set-up-pages-with-a-custom-domain)
My example is here [**https://gabriellourenco.com/**](https://gabriellourenco.com/)

### Google Analytics
You can monitor your resume setting [***Google Analytics***](https://analytics.google.com/) just change ***ID*** in the [project index](index.mustache#L4)

### License©️
Default theme designed by [**Franklin Schamhart**](https://dribbble.com/shots/1887983-Resume).

Resume is licensed under the MIT Open Source license. For more information, 
see the LICENSE file in this repository.

> Spanish version: I am a native Spanish speaker if you wish can see this 
documentation in [**ES-README**](ES-README.md)
